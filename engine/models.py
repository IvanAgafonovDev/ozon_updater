from django.db import models
from shops.models import Shop


class ShopUpdateLog(models.Model):
    """Create an instance for every shop in the DB when Celery triggers periodic task 'update_shops_task' (15 min)."""
    shop =                   models.ForeignKey(Shop, on_delete=models.CASCADE)
    datetime =               models.DateTimeField(auto_now_add=True)

    is_feed_downloaded =     models.BooleanField(default=False)
    is_feed_downloaded_exc = models.TextField(null=True)

    is_feed_cleaned =        models.BooleanField(default=False)
    is_feed_cleaned_exc =    models.TextField(null=True)

    def __str__(self):
        return f'{self.shop.name} ({self.datetime})'
