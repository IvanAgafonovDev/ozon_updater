from __future__ import absolute_import, unicode_literals

import traceback
from datetime import timedelta
from celery.task import periodic_task
from .update import update_shops


@periodic_task(run_every=(timedelta(minutes=1)), name='update_shops_task')
def update_shops_task():
    """Trigger all update processes of ozon_updater project for every shop in the database (multithreadingly)."""

    try:
        update_shops()
        return f'UPDATE IS SUCCESSFUL'

    except Exception:
        return f'UPDATE IS FAILED: {traceback.format_exc()}'
