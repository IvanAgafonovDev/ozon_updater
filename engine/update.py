import traceback
from threading import Thread

from shops.models import Shop
from shops.utils import feed_cleaner, feed_downloader

from .models import ShopUpdateLog


def _update_shop(name: str, feed: str):
    """Run multithreadingly, handle all update logic for each shop and log it.

    Every logic step of whole update process runs within a try-except block and
    also logging happens there depends on resutls.
    """
    shop_update_log = ShopUpdateLog.objects.create(shop=Shop.objects.get(name=name))

    # Download feed
    try:
        feed_downloader(shop_name=name, feed_link=feed)
        shop_update_log.is_feed_downloaded = True
        shop_update_log.save()
    except Exception:
        shop_update_log.is_feed_downloaded_exc = traceback.format_exc()
        shop_update_log.save()
        return

    # Clean feed
    try:
        feed_cleaner(shop_name=name, feed_ext=feed.rsplit(".", 1)[-1])  # feed_ext usually is xml or json, csv and etc.
        shop_update_log.is_feed_cleaned = True
        shop_update_log.save()
    except Exception:
        shop_update_log.is_feed_cleaned_exc = traceback.format_exc()
        shop_update_log.save()
        return


def update_shops():
    """Get all shops from the database and start a separate thread with all updating machinery for every shop."""

    shops = Shop.objects.all()
    threads = []

    for shop in shops:
        threads.append(Thread(target=_update_shop, kwargs={'name': shop.name, 'feed': shop.feed}))
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()
