import importlib, os, time
import requests
from pathlib import Path


CURR_APP_PATH = os.path.dirname(os.path.realpath(__file__))


class OpenLock:
    """It's a context manager class which implements standart read/write file stuff but with custom lock.

    POSIX doesn't set a lock on opened files but sometimes it can be very usefull. When you open a file with this CM it
    creates a lock-file next to the opened file. Than nobody can get access to the file (using this CM) till closed.

    Don't use it before completely understanding what happening!
    """
    def __init__(self, file_path, mode='r', get_lock_attempts=0):

        self.file_path = file_path
        self.mode = mode
        self.file = None
        # The lock-file is next to a opened file (will see pair: file.xml and file.xml_lock)
        self.lock_path = str(Path(self.file_path).parent) + f'/{str(Path(self.file_path).name)}_lock'
        self.get_lock_attempts = get_lock_attempts

    @property
    def is_locked(self):
        """If no lock-file it creates this and sets its value to 0 or returns value of the existing lock-file."""

        if os.path.exists(self.lock_path):
            with open(self.lock_path) as lock:
                return int(lock.read())
        else:
            with open(self.lock_path, 'w') as lock:
                lock.write('0')
                return 0

    def lock_file(self):
        with open(self.lock_path, 'w') as lock:
            lock.write('1')

    def unlock_file(self):
        with open(self.lock_path, 'w') as lock:
            lock.write('0')

    def __enter__(self):
        # If a file locked by this CM from another place we wait for 100 sec or AssertionError.
        while self.is_locked == 1:
            if self.get_lock_attempts == 100:
                raise AssertionError("Can't get read/write access to the file. Probably the deadlock issues.")
            self.get_lock_attempts += 1
            time.sleep(1)

        self.lock_file()
        self.file = open(self.file_path, self.mode)
        return self.file

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.file.close()
        self.unlock_file()


def feed_cleaner(shop_name: str, feed_ext: str):
    """This function dynamically imports custom clean function written for every shop manually.

    Than it calls this func and it's the last similarities. All clean() funcs work differently
    so all we have to know is we will get cleaned_feed.json in all/shop_name/feed_tmp folder after executing.
    """

    downloaded_feed_path = CURR_APP_PATH + f'/all/{shop_name}/feed_tmp/downloaded_feed.{feed_ext}'

    # Dynamic clean() function import for every shop in the system.
    clean = __import__(f'shops.all.{shop_name}.cleaner', globals(), locals(), ['clean']).clean

    with OpenLock(file_path=downloaded_feed_path, mode='r') as downloaded_feed:  # Why OpenLock? Read its docstring.
        clean(downloaded_feed)


def feed_downloader(shop_name: str, feed_link: str) -> str:
    """Download a shop feed (has 10 tries in case of network problems) and saves it in a all.{shopname} feed folder.

    If success it returns path: str where feed was downloaded (mostly for testing purposes).
    If can't get a feed it raises AssertionError and this will be handled and logged at the caller's level.
    """

    for _ in range(10):

        r = requests.get(feed_link)

        if r.status_code == 200:
            file_path = CURR_APP_PATH + f'/all/{shop_name}/feed_tmp/downloaded_feed.{feed_link.rsplit(".", 1)[-1]}'
            with OpenLock(file_path=file_path, mode='w') as file:  # OpenLock is a simple context manager
                file.write(r.text)
            return file_path
        else:
            time.sleep(10)

    else:
        raise AssertionError('The feed was not uploaded.')
