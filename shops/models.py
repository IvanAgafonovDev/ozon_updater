from django.db import models


class Shop(models.Model):
    """Don't forget to add a new shop manually in the Django shell."""

    name = models.CharField(unique=True, max_length=25)
    feed = models.CharField(unique=True, max_length=250)

    def __str__(self):
        return f'{self.name} ({self.id})'
