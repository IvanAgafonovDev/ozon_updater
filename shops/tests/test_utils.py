import os, unittest
from dataclasses import dataclass
from typing import List
from unittest.mock import Mock

from shops.utils import CURR_APP_PATH, OpenLock, feed_cleaner, feed_downloader


class TestOpenLock(unittest.TestCase):

    test_file_path = CURR_APP_PATH + '/tests/file'
    test_lock_path = CURR_APP_PATH + '/tests/file_lock'

    @classmethod
    def setUpClass(cls):
        with open(cls.test_file_path, 'w') as test_file:
            test_file.write('test1')

    @classmethod
    def tearDown(cls):
        os.remove(cls.test_file_path)
        os.remove(cls.test_lock_path)

    def test_lock_exists(self):

        with open(self.test_lock_path, 'w') as test_lock:
            test_lock.write('0')

        open_lock = OpenLock(file_path=self.test_file_path, mode='w')

        with open_lock as test_file:
            self.assertEqual(open_lock.is_locked, 1)
            test_file.write('test2')
            self.assertEqual(open_lock.is_locked, 1)
        self.assertEqual(open_lock.is_locked, 0)

    def test_lock_will_be_created(self):

        open_lock = OpenLock(file_path=self.test_file_path, mode='w')

        with open_lock as test_file:
            self.assertEqual(open_lock.is_locked, 1)
            test_file.write('test2')
            self.assertEqual(open_lock.is_locked, 1)
        self.assertEqual(open_lock.is_locked, 0)

    def test_double_access_to_locked_file(self):

        open_lock = OpenLock(file_path=self.test_file_path)
        open_lock2 = OpenLock(file_path=self.test_file_path, mode='w', get_lock_attempts=100)
        # get_lock_attempts=100 cause by default it's 0 so we have to wait 100 sec (=attempts) to raise AssertionError

        with open_lock as test_file1:
            test_file1.read()
            with self.assertRaises(AssertionError):
                with open_lock2 as test_file2:
                    test_file2.write('test2')

        with open(self.test_file_path, 'r') as test_file:
            self.assertEqual(test_file.read(), 'test1')


class TestFeedDownloader(unittest.TestCase):

    @dataclass
    class Shop:
        name: str
        feed: str

    shops: List[Shop] = [
        Shop(name='mvideo', feed='http://mvideo-ozon.ecom-seller.ru/static/xml/mbt.xml'),
        Shop(name='kolesa-darom', feed='https://feed-new.kolesa-darom.ru/feed/live/moskva/ozon_central_wh.xml'),
        Shop(name='any-name', feed='any-link-with-format-at-the-end.csv'),
    ]
    # All stuff above is just a dummy data so no use real database queries

    @unittest.mock.patch('shops.utils.open')
    @unittest.mock.patch('shops.utils.requests.get')
    def test_feed_downloaded_saved(self, mocked_requests_get, mocked_open):
        """Fail in case of utils.feed_downloader returns an incorrect file path which was downloaded."""

        mock_requests_get = Mock()
        mock_requests_get.status_code = 200
        mocked_requests_get.return_value = mock_requests_get

        mocked_open.return_value.__enter__.write = Mock()

        for shop in self.shops:
            t_path = CURR_APP_PATH + f'/all/{shop.name}/feed_tmp/downloaded_feed.'
            t_path = t_path + f'{shop.feed.rsplit(".", 1)[-1]}' + '_test'  # add extension of a file + '_test'

            r_path = feed_downloader(shop_name=shop.name, feed_link=shop.feed + '_test')

            # /home/user/ozon_updater/shops/all/kolesa-darom/feed_tmp/downloaded_feed.xml_test
            self.assertEqual(t_path, r_path)

    @unittest.mock.patch('shops.utils.time.sleep')
    @unittest.mock.patch('shops.utils.requests.get')
    def test_response_is_not_200(self, mocked_requests_get, mocked_time_sleep):
        """Fail in case of the response different than 200 but no AssertionError raised."""

        mock_requests_get = Mock()
        mock_requests_get.status_code = 404
        mocked_requests_get.return_value = mock_requests_get

        mocked_time_sleep.return_value = None

        for shop in self.shops:
            with self.assertRaises(AssertionError):
                feed_downloader(shop_name=shop.name, feed_link=shop.feed)


class TestFeedCleaner(unittest.TestCase):
    def test_feed_cleaner(self):
        """Don't forget to write this test!"""
        self.assertEqual(0, 1)
